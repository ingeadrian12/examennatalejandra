package com.nat.examen.api

import com.nat.examen.domain.model.LoginResponse
import retrofit2.http.*

interface WebService {

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("oauth/token")

   suspend fun login(@Field("grant_type") grant_type:String,
                     @Field("username") username:String,
                     @Field("password") password:String):LoginResponse
}