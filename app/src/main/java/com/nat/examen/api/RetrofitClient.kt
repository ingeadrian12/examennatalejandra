package com.nat.examen.api

import com.google.gson.GsonBuilder
import com.nat.examen.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {
    val webService by lazy {

        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(makeOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(WebService::class.java)
    }

    private fun makeOkHttpClient(): OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor(AuthInterceptor())
            .addNetworkInterceptor(makeLoggingInterceptor())
            .connectTimeout(90, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .writeTimeout(90, TimeUnit.SECONDS)
            .build()
    }

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }
}