package com.nat.examen.api

import com.nat.examen.utils.Constants
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor:Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader(
            "Authorization",
            "Basic ${Constants.TOKEN}"
        ).build()
        return chain.proceed(request)
    }
}