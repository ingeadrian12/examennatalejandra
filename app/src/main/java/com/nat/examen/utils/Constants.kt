package com.nat.examen.utils

import android.content.Context
import android.content.SharedPreferences
import com.nat.examen.App

object Constants {
    const val BASE_URL = "https://uat.firmaautografa.com/authorization-server/"
    const val TOKEN ="Wm1Ga0xXTXlZeTF3YjNKMFlXdz06TWpoa04yUTNNbUppWVRWbVpHTTBObVl4Wmpka1lXSmpZbVEyTmpBMVpEVXpaVFZoT1dNMVpHVTROakF4TldVeE9EWmtaV0ZpTnpNd1lUUm1ZelV5WWc9PQ=='\\"
}
object SharedPreferencesManager{
    private const val APP_SETTING_FILE = "APP_FILE"

    private fun getSharedPreferences(): SharedPreferences {
        return App.instance.applicationContext.getSharedPreferences(APP_SETTING_FILE, Context.MODE_PRIVATE)
    }

    fun setSomeStringValue(valueLabel: String?, dataValue: String?) {
        val editor = getSharedPreferences().edit()
        editor.putString(valueLabel, dataValue)
        editor.apply()
    }


    fun getSomeStringValue(valueLabel: String?): String? {
        return getSharedPreferences().getString(valueLabel, "")
    }

}