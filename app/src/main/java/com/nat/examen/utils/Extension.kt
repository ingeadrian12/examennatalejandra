package com.nat.examen.utils

import java.security.MessageDigest

fun String.sha256(input: String): String {

    return MessageDigest
        .getInstance("SHA-256")
        .digest(input.toByteArray())
        .fold("", { str, it -> str + "%02x".format(it) })
}