package com.nat.examen.domain.remote

import com.nat.examen.api.WebService
import com.nat.examen.domain.model.LoginResponse
import com.nat.examen.domain.model.Services


class LoginDataSource(private val webService: WebService)  {


    suspend fun login( grant_type:String,
                       username:String,
                       password:String):LoginResponse = webService.login(grant_type,username,password)

     fun getServicesClaro() = listOf<Services>(Services("claro"),Services("claro"),Services("claro"),
        Services("claro"),Services("claro"))


     fun getServicesEntel() = listOf<Services>(Services("Entel"),Services("Entel"),Services("Entel"),
        Services("Entel"),Services("Entel"))

     fun getServicesTuenti() = listOf<Services>(Services("tuenti"),Services("tuenti"),Services("tuenti"),
        Services("tuenti"),Services("tuenti"))
}