package com.nat.examen.domain.model

data class LoginResponse(
    val access_token: String,
    val expires_in: Int,
    val jti: String,
    val refresh_token: String,
    val scope: String,
    val token_type: String
)