package com.nat.examen.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.nat.examen.core.Resource
import com.nat.examen.repository.Repository
import kotlinx.coroutines.Dispatchers

class FragmentDetailViewModel(private val repository: Repository):ViewModel() {

    fun getServices() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())

        try {
            emit(Resource.Success(Triple(repository.getServicesClaro(),repository.getServicesEntel(),repository.getServicesTuenti())))
        }
        catch (e:Exception){
            emit(Resource.Failure(e))
        }
    }
}

class FragmentDetailViewModelFactory(private val repo: Repository): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(Repository::class.java).newInstance(repo)
    }

}