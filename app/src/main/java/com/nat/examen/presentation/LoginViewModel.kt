package com.nat.examen.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.nat.examen.core.Resource
import com.nat.examen.repository.Repository
import kotlinx.coroutines.Dispatchers

class LoginViewModel(private val repo: Repository):ViewModel() {

    fun fetchLogin( grant_type:String, username:String, password:String)= liveData(Dispatchers.IO) {
        emit(Resource.Loading())

        try {
            emit(Resource.Success(repo.login(grant_type,username,password)))
        }
        catch (e:Exception){
            emit(Resource.Failure(e))
        }
    }

}

class LoginViewModelFactory(private val repo: Repository):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
      return modelClass.getConstructor(Repository::class.java).newInstance(repo)
    }

}