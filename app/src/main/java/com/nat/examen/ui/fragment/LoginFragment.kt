package com.nat.examen.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.nat.examen.R
import com.nat.examen.api.RetrofitClient
import com.nat.examen.core.Resource
import com.nat.examen.databinding.FragmentLoginBinding
import com.nat.examen.domain.remote.LoginDataSource
import com.nat.examen.presentation.LoginViewModel
import com.nat.examen.presentation.LoginViewModelFactory
import com.nat.examen.repository.RepositoryImplents
import com.nat.examen.utils.SharedPreferencesManager
import com.nat.examen.utils.sha256


class LoginFragment : Fragment(R.layout.fragment_login) {

    private lateinit var binding: FragmentLoginBinding
    private val  viewModel by viewModels<LoginViewModel> {
        LoginViewModelFactory(
            RepositoryImplents(
            LoginDataSource(RetrofitClient.webService)
            )
        )  }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
          binding = FragmentLoginBinding.bind(view)
        (activity as AppCompatActivity?)!!.supportActionBar?.hide()

        binding.button2.setOnClickListener {
            gotoLogin(binding.editUser.text.toString(),binding.editpass.text.toString())
        }
    }

    private fun gotoLogin(user:String,pass:String){

        viewModel.fetchLogin("password",user,
                String().sha256(pass)).observe(viewLifecycleOwner, Observer {

            when(it){
                is Resource.Loading ->{
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressBar.visibility = View.GONE
                    SharedPreferencesManager.setSomeStringValue("access_token",it.data.access_token)
                    SharedPreferencesManager.setSomeStringValue("refresh_token",it.data.refresh_token)
                    findNavController().navigate(R.id.action_loginFragment_to_detailFragment3)

                }
                is Resource.Failure ->{
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(activity?.applicationContext,"Error",Toast.LENGTH_LONG).show()
                }
            }

        })
    }

}