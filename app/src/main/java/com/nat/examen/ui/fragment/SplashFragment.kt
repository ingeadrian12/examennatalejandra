package com.nat.examen.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.nat.examen.R
import com.nat.examen.databinding.FragmentSplashBinding
import com.nat.examen.ui.MainActivity


class SplashFragment : Fragment(R.layout.fragment_splash) {

    private val SPLASH_TIME_OUT:Long = 3000
   private lateinit var binding: FragmentSplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSplashBinding.bind(view)
        (activity as AppCompatActivity?)!!.supportActionBar?.hide()
        Handler(Looper.getMainLooper()).postDelayed({
            findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
        }, SPLASH_TIME_OUT)

    }

}