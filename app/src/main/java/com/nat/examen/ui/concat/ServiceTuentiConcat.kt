package com.nat.examen.ui.concat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nat.examen.core.BaseConcatHolder
import com.nat.examen.databinding.ServiceRowTuentiBinding
import com.nat.examen.ui.adapter.AdapterViewSelect

class ServiceTuentiConcat(private val adapterViewSelect: AdapterViewSelect):RecyclerView.Adapter<BaseConcatHolder<*>>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseConcatHolder<*> {
       val binding = ServiceRowTuentiBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val concatHolder = HolderViewTenti(binding)
        return  concatHolder
    }

    override fun onBindViewHolder(holder: BaseConcatHolder<*>, position: Int) {
      when(holder){

          is HolderViewTenti -> holder.bind(adapterViewSelect)
      }
    }

    override fun getItemCount(): Int = 1



    private inner class HolderViewTenti(private val binding: ServiceRowTuentiBinding):BaseConcatHolder<AdapterViewSelect>(binding.root){
        override fun bind(adapter: AdapterViewSelect) {
           binding.rvServicesTuenti.adapter = adapter
        }

    }
}