package com.nat.examen.ui.concat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nat.examen.core.BaseConcatHolder
import com.nat.examen.databinding.ServiceRowEntelBinding
import com.nat.examen.ui.adapter.AdapterViewSelect

class ServiceEntelConcat(private val adapterViewSelect: AdapterViewSelect):RecyclerView.Adapter<BaseConcatHolder<*>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseConcatHolder<*> {
        val binding = ServiceRowEntelBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val holderViewEntelConcat = HolderViewEntelConcat(binding)
        return  holderViewEntelConcat
    }

    override fun onBindViewHolder(holder: BaseConcatHolder<*>, position: Int) {
       when (holder){
           is  HolderViewEntelConcat -> holder.bind(adapterViewSelect)
       }
    }

    override fun getItemCount(): Int =1

    private inner class HolderViewEntelConcat(private val binding: ServiceRowEntelBinding):BaseConcatHolder<AdapterViewSelect>(binding.root){
        override fun bind(item: AdapterViewSelect) {
           binding.rvServicesEntel.adapter = item
        }

    }
}