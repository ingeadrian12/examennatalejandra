package com.nat.examen.ui.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.nat.examen.R
import com.nat.examen.api.RetrofitClient
import com.nat.examen.core.Resource
import com.nat.examen.databinding.FragmentDetailBinding
import com.nat.examen.domain.model.Services
import com.nat.examen.domain.remote.LoginDataSource
import com.nat.examen.presentation.FragmentDetailViewModel
import com.nat.examen.presentation.LoginViewModelFactory
import com.nat.examen.repository.RepositoryImplents
import com.nat.examen.ui.adapter.AdapterViewSelect
import com.nat.examen.ui.concat.ServiceEntelConcat
import com.nat.examen.ui.concat.ServiceTuentiConcat
import com.nat.examen.ui.concat.ServicesAdapterConcat


class DetailFragment : Fragment(R.layout.fragment_detail),AdapterViewSelect.OnServiceclickListener {

    private lateinit var concatAdapter: ConcatAdapter
    private lateinit var binding : FragmentDetailBinding
    private val  viewModel by viewModels<FragmentDetailViewModel> {
        LoginViewModelFactory(
            RepositoryImplents(
                LoginDataSource(RetrofitClient.webService)
            )
        )  }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailBinding.bind(view)
        concatAdapter = ConcatAdapter()
        viewModel.getServices().observe(viewLifecycleOwner, Observer {  result ->

            when(result){

                is Resource.Loading ->{
                    binding.progress.visibility = View.VISIBLE

                }
                is  Resource.Success ->{

                    binding.progress.visibility = View.GONE
                    concatAdapter.apply {
                        addAdapter(0,ServicesAdapterConcat(AdapterViewSelect(result.data.first,this@DetailFragment)))
                        addAdapter(1,ServiceEntelConcat(AdapterViewSelect(result.data.second,this@DetailFragment)))
                        addAdapter(2,ServiceTuentiConcat(AdapterViewSelect(result.data.third,this@DetailFragment)))
                    }
                    binding.listService.adapter = concatAdapter
                }

                is Resource.Failure ->{
                    binding.progress.visibility = View.GONE
                }
            }

        })

    }

    override fun onclick(services: Services) {
        findNavController().navigate(R.id.action_detailFragment_to_recargasFragment)
    }

}