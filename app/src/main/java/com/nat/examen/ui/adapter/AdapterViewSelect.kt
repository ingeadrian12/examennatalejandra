package com.nat.examen.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.nat.examen.R
import com.nat.examen.core.BaseViewHolder
import com.nat.examen.databinding.ServiceItemBinding
import com.nat.examen.domain.model.Services
import com.nat.examen.ui.fragment.DetailFragment


class AdapterViewSelect(val services: List<Services>, private val onclick: OnServiceclickListener):RecyclerView.Adapter<BaseViewHolder<*>> (){

    interface OnServiceclickListener{
        fun onclick(services: Services )
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val binding = ServiceItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val viewHolderRecargas=ViewHolderRecargas(binding,parent.context)
        binding.root.setOnClickListener {
            val position = viewHolderRecargas.bindingAdapterPosition.takeIf { it != DiffUtil.DiffResult.NO_POSITION } ?: return@setOnClickListener
            onclick.onclick(services[position])
        }
        return viewHolderRecargas
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
     when(holder){
         is ViewHolderRecargas -> holder.bind(services[position])

     }
    }

    override fun getItemCount(): Int = services.size

   private inner  class ViewHolderRecargas(val binding:ServiceItemBinding,val context: Context):BaseViewHolder<Services>(binding.root) {
       override fun bind(item: Services) {
        when(item.services_name){
             "claro" ->{
                binding.imageService.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_claro))
            }
            "Entel" ->{
                binding.imageService.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_entel))
            }
            "tuenti" ->{
                binding.imageService.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_tuenti))

            }

        }
       }

   }
}