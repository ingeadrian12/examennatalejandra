package com.nat.examen.ui.concat
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nat.examen.core.BaseConcatHolder
import com.nat.examen.databinding.ServiceRowBinding
import com.nat.examen.ui.adapter.AdapterViewSelect

class ServicesAdapterConcat(private val adapter: AdapterViewSelect):RecyclerView.Adapter<BaseConcatHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseConcatHolder<*> {
        val binding = ServiceRowBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val concatHolder = ConcatViewHolder(binding)
        return  concatHolder
    }

    override fun onBindViewHolder(holder: BaseConcatHolder<*>, position: Int) {
      when(holder){
          is ConcatViewHolder ->holder.bind(adapter)
      }
    }

    override fun getItemCount(): Int =1


    private inner class ConcatViewHolder(private val binding: ServiceRowBinding):BaseConcatHolder<AdapterViewSelect>(binding.root){
        override fun bind(adapter: AdapterViewSelect) {
            binding.rvServices.adapter = adapter

        }

    }
}