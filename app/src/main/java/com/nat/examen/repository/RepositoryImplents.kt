package com.nat.examen.repository

import com.nat.examen.domain.model.LoginResponse
import com.nat.examen.domain.model.Services
import com.nat.examen.domain.remote.LoginDataSource

class RepositoryImplents(private val loginDataSource: LoginDataSource): Repository
{
    override suspend fun login( grant_type:String,
                                username:String,
                                password:String): LoginResponse = loginDataSource.login(grant_type, username, password)

    override suspend fun getServicesClaro(): List<Services> =loginDataSource.getServicesClaro()
    override suspend fun getServicesTuenti(): List<Services> = loginDataSource.getServicesTuenti()

    override suspend fun getServicesEntel(): List<Services> = loginDataSource.getServicesEntel()

}