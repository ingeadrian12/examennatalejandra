package com.nat.examen.repository

import com.nat.examen.domain.model.LoginResponse
import com.nat.examen.domain.model.Services

interface Repository {
    suspend fun login( grant_type:String,
                       username:String,
                       password:String):LoginResponse

    suspend fun getServicesClaro():List<Services>
    suspend fun getServicesTuenti():List<Services>
    suspend fun getServicesEntel():List<Services>
}
